use image::GenericImageView;
use noise::BasicPerlin;
use std::path::Path;
use vec3::Vec3;

pub trait Texture {
    fn value(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        unimplemented!();
    }

    fn box_clone(&self) -> Box<dyn Texture>;
}

impl Clone for Box<dyn Texture> {
    fn clone(&self) -> Box<dyn Texture> {
        self.box_clone()
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Color {
    pub color: Vec3,
}

impl Color {
    pub fn new_from_vec3(v: &Vec3) -> Self {
        Self {
            color: Vec3::new_from_vec3(&v),
        }
    }

    pub fn new() -> Self {
        Self {
            color: Vec3::new_empty(),
        }
    }

    pub fn new_from_color(c: &Color) -> Self {
        Color::new_from_vec3(c.get_color())
    }

    pub fn get_color(&self) -> &Vec3 {
        &self.color
    }
}

impl Texture for Color {
    fn value(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        self.color.clone()
    }

    fn box_clone(&self) -> Box<dyn Texture> {
        Box::new(self.clone())
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Checker {
    odd: Color,
    even: Color,
}

impl Checker {
    pub fn new_from_vec3(odd: &Color, even: &Color) -> Self {
        Self {
            odd: Color::new_from_color(odd),
            even: Color::new_from_color(even),
        }
    }

    pub fn new() -> Self {
        Self {
            odd: Color::new(),
            even: Color::new(),
        }
    }

    pub fn new_from_checker(c: Checker) -> Self {
        Checker::new_from_vec3(c.get_odd(), c.get_even())
    }

    fn get_even(&self) -> &Color {
        &self.even
    }

    fn get_odd(&self) -> &Color {
        &self.odd
    }
}

impl Texture for Checker {
    fn value(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        let sines = (10. * p.x()).sin() * (10. * p.y()).sin() * (10. * p.z()).sin();
        let res: Vec3;
        if sines < 0. {
            res = self.odd.value(u, v, p);
        } else {
            res = self.even.value(u, v, p);
        }
        res
    }

    fn box_clone(&self) -> Box<dyn Texture> {
        Box::new(self.clone())
    }
}

#[derive(Copy, Clone)]
pub struct NoiseTexture {
    perlin: BasicPerlin,
}

impl NoiseTexture {
    pub fn new() -> Self {
        NoiseTexture {
            perlin: BasicPerlin::new(),
        }
    }
}

impl Texture for NoiseTexture {
    fn value(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        let scale = 2f32;
        // Vec3::new(1.,1.,1.) * self.perlin.noise(&(scale * *p))
        Vec3::new(1., 1., 1.) * self.perlin.turb(&(scale * *p), 7)
    }

    fn box_clone(&self) -> Box<dyn Texture> {
        Box::new(self.clone())
    }
}

#[derive(Copy, Clone)]
pub struct MarbleTexture {
    perlin: BasicPerlin,
}

impl MarbleTexture {
    pub fn new() -> Self {
        MarbleTexture {
            perlin: BasicPerlin::new(),
        }
    }
}

impl Texture for MarbleTexture {
    fn value(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        let scale = 4f32;
        // Vec3::new(1.,1.,1.) * self.perlin.turb(&(scale * *p), 7)
        Vec3::new(1f32, 1f32, 1f32)
            * 0.5
            * (1f32 + (scale * p.z() + 10f32 * self.perlin.turb(p, 7))).sin()
    }

    fn box_clone(&self) -> Box<dyn Texture> {
        Box::new(self.clone())
    }
}

#[derive(Clone)]
pub struct SphereImageTexture {
    image: image::RgbImage,
}

impl SphereImageTexture {
    pub fn new(path: &str) -> Self {
        SphereImageTexture {
            image: image::open(path).unwrap().as_rgb8().unwrap().clone(),
        }
    }
}

impl Texture for SphereImageTexture {
    fn value(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        let (nx, ny) = self.image.dimensions();
        let mut i = (u) * nx as f32;
        let mut j = (1f32 - v) * ny as f32 - 0.001;
        // println!("{} {}",u,v);
        if i < 0f32 {
            i = 0f32;
        }
        if j < 0f32 {
            j = 0f32;
        }
        if i > nx as f32 - 1f32 {
            i = nx as f32 - 1f32;
        }
        if j > ny as f32 - 1f32 {
            j = ny as f32 - 1f32;
        }
        let pixel = self.image.get_pixel(i as u32, j as u32);
        Vec3::new(
            pixel.0[0] as f32 / 255f32,
            pixel.0[1] as f32 / 255f32,
            pixel.0[2] as f32 / 255f32,
        )
    }

    fn box_clone(&self) -> Box<dyn Texture> {
        Box::new(self.clone())
    }
}
