use serde::{Deserialize, Serialize};
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Div;
use std::ops::DivAssign;
use std::ops::Index;
use std::ops::Mul;
use std::ops::MulAssign;
use std::ops::Neg;
use std::ops::Sub;
use std::ops::SubAssign;

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Vec3 {
    vec: [f32; 3],
}

impl Vec3 {
    pub const fn new_const(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3 { vec: [x, y, z] }
    }

    pub fn new(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3 { vec: [x, y, z] }
    }

    pub fn new_empty() -> Vec3 {
        Vec3 { vec: [0., 0., 0.] }
    }

    pub fn new_from_vec3(other: &Vec3) -> Vec3 {
        Vec3::new(other.x(), other.y(), other.z())
    }

    pub fn at(&self, index: usize) -> f32 {
        match index {
            1 => self.x(),
            2 => self.y(),
            3 => self.z(),
            _ => self.x(),
        }
    }

    pub fn x(&self) -> f32 {
        self.vec[0]
    }

    pub fn y(&self) -> f32 {
        self.vec[1]
    }

    pub fn z(&self) -> f32 {
        self.vec[2]
    }

    pub fn r(&self) -> f32 {
        self.vec[0]
    }

    pub fn g(&self) -> f32 {
        self.vec[1]
    }

    pub fn b(&self) -> f32 {
        self.vec[2]
    }

    pub fn length(&self) -> f32 {
        (self.vec[0] * self.vec[0] + self.vec[1] * self.vec[1] + self.vec[2] * self.vec[2]).sqrt()
    }

    pub fn squared_length(&self) -> f32 {
        self.vec[0] * self.vec[0] + self.vec[1] * self.vec[1] + self.vec[2] * self.vec[2]
    }

    pub fn unit_vector(&self) -> Vec3 {
        let v = Vec3::new(self.vec[0], self.vec[1], self.vec[2]);
        let val = self.length();
        let res = v / val;
        res
    }

    #[allow(dead_code)]
    fn dot(&self, other: Vec3) -> f32 {
        let aux = *self * other;
        aux[0] + aux[1] + aux[2]
    }

    #[allow(dead_code)]
    fn cross_vector(&self, other: Vec3) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[1] * other.vec[2] - self.vec[2] * self.vec[1],
                -(self.vec[0] * other.vec[2] - self.vec[2] * self.vec[0]),
                self.vec[0] * other.vec[1] - self.vec[1] * self.vec[0],
            ],
        }
    }

    pub fn print_debug(&self) {
        let mut print = false;
        self.vec.iter().for_each(|x| {
            if *x >= 255. {
                print = true;
            }
        });
        if print {
            self.vec.iter().for_each(|x| print!("{} ", x));
            println!();
        };
    }

    pub fn clamp(&mut self, min: f32, max: f32) {
        self.vec.iter_mut().for_each(|x| *x = clamp(*x, min, max));
    }

    pub fn sqrt(&mut self) {
        self.vec[0] = self.vec[0].sqrt();
        self.vec[1] = self.vec[1].sqrt();
        self.vec[2] = self.vec[2].sqrt();
    }

    pub fn map(self, mut f: impl FnMut(f32) -> f32) -> Self {
        Vec3 {
            vec: [f(self.vec[0]), f(self.vec[1]), f(self.vec[2])],
        }
    }
}

#[inline]
fn clamp(value: f32, min: f32, max: f32) -> f32 {
    assert!(min <= max);
    let mut x = value;
    if x < min {
        x = min;
    }
    if x > max {
        x = max;
    }
    x
}

pub fn dot(v1: &Vec3, v2: &Vec3) -> f32 {
    v1.vec[0] * v2.vec[0] + v1.vec[1] * v2.vec[1] + v1.vec[2] * v2.vec[2]
}

pub fn cross(v1: &Vec3, v2: &Vec3) -> Vec3 {
    Vec3::new(
        v1.vec[1] * v2.vec[2] - v1.vec[2] * v2.vec[1],
        (v1.vec[0] * v2.vec[2] - v1.vec[2] * v2.vec[0]) * -1.,
        v1.vec[0] * v2.vec[1] - v1.vec[1] * v2.vec[0],
    )
}

pub fn reflect(v: &Vec3, n: &Vec3) -> Vec3 {
    *v - (*n * dot(v, n) * 2.)
}

pub fn refract(v: &Vec3, n: &Vec3, ni_over_nt: f32, refracted: &mut Vec3) -> bool {
    let uv = v.clone().unit_vector();
    let dt = dot(&uv, &n.clone());
    let discriminant = 1. - ni_over_nt * ni_over_nt * (1. - dt * dt);
    if discriminant > 0. {
        *refracted = (uv - n.clone() * dt) * ni_over_nt - n.clone() * discriminant.sqrt();
        true
    } else {
        false
    }
}

pub fn schlick(cos: f32, ref_index: f32) -> f32 {
    let mut r0 = (1. - ref_index) / (1. + ref_index);
    r0 = r0 * r0;
    r0 + (1. + r0) * (1. - cos).powf(5.)
}

impl Add<Vec3> for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] + other.vec[0],
                self.vec[1] + other.vec[1],
                self.vec[2] + other.vec[2],
            ],
        }
    }
}

impl Add<i32> for Vec3 {
    type Output = Vec3;

    fn add(self, other: i32) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] + other as f32,
                self.vec[1] + other as f32,
                self.vec[2] + other as f32,
            ],
        }
    }
}

impl Add<f32> for Vec3 {
    type Output = Vec3;

    fn add(self, other: f32) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] + other as f32,
                self.vec[1] + other as f32,
                self.vec[2] + other as f32,
            ],
        }
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            vec: [
                self.vec[0] + other.vec[0],
                self.vec[1] + other.vec[1],
                self.vec[2] + other.vec[2],
            ],
        };
    }
}

impl Div<Vec3> for Vec3 {
    // The division of rational numbers is a closed operation.
    type Output = Vec3;

    fn div(self, other: Vec3) -> Vec3 {
        // When less lazy replace with an actual verification
        // if rhs.vec[0] == 0 ||  {
        //     panic!("Cannot divide by zero-valued `Rational`!");
        // }
        Vec3 {
            vec: [
                self.vec[0] / other.vec[0],
                self.vec[1] / other.vec[1],
                self.vec[2] / other.vec[2],
            ],
        }
    }
}

impl Div<f32> for Vec3 {
    // The division of rational numbers is a closed operation.
    type Output = Vec3;

    fn div(self, other: f32) -> Vec3 {
        // When less lazy replace with an actual verification
        // if rhs.vec[0] == 0 ||  {
        //     panic!("Cannot divide by zero-valued `Rational`!");
        // }
        Vec3 {
            vec: [
                self.vec[0] / other,
                self.vec[1] / other,
                self.vec[2] / other,
            ],
        }
    }
}

impl Div<Vec3> for f32 {
    // The division of rational numbers is a closed operation.
    type Output = Vec3;

    fn div(self, other: Vec3) -> Vec3 {
        // When less lazy replace with an actual verification
        // if rhs.vec[0] == 0 ||  {
        //     panic!("Cannot divide by zero-valued `Rational`!");
        // }
        Vec3 {
            vec: [self / other[0], self / other[1], self / other[2]],
        }
    }
}

impl DivAssign<Vec3> for Vec3 {
    fn div_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            vec: [
                self.vec[0] / other.vec[0],
                self.vec[1] / other.vec[1],
                self.vec[2] / other.vec[2],
            ],
        };
    }
}

impl DivAssign<u32> for Vec3 {
    fn div_assign(&mut self, other: u32) {
        *self = Vec3 {
            vec: [
                self.vec[0] / other as f32,
                self.vec[1] / other as f32,
                self.vec[2] / other as f32,
            ],
        };
    }
}

impl Mul<Vec3> for Vec3 {
    // The multiplication of rational numbers is a closed operation.
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] * other.vec[0],
                self.vec[1] * other.vec[1],
                self.vec[2] * other.vec[2],
            ],
        }
    }
}

impl Mul<f32> for Vec3 {
    // The multiplication of rational numbers is a closed operation.
    type Output = Vec3;

    fn mul(self, other: f32) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] * other,
                self.vec[1] * other,
                self.vec[2] * other,
            ],
        }
    }
}

impl Mul<i8> for Vec3 {
    // The multiplication of rational numbers is a closed operation.
    type Output = Vec3;

    fn mul(self, other: i8) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] * other as f32,
                self.vec[1] * other as f32,
                self.vec[2] * other as f32,
            ],
        }
    }
}

impl Mul<Vec3> for f32 {
    // The division of rational numbers is a closed operation.
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
        // When less lazy replace with an actual verification
        // if rhs.vec[0] == 0 ||  {
        //     panic!("Cannot divide by zero-valued `Rational`!");
        // }
        Vec3 {
            vec: [self * other[0], self * other[1], self * other[2]],
        }
    }
}

impl MulAssign<Vec3> for Vec3 {
    fn mul_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            vec: [
                self.vec[0] * other.vec[0],
                self.vec[1] * other.vec[1],
                self.vec[2] * other.vec[2],
            ],
        };
    }
}

impl Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, other: Vec3) -> Vec3 {
        Vec3 {
            vec: [
                self.vec[0] - other.vec[0],
                self.vec[1] - other.vec[1],
                self.vec[2] - other.vec[2],
            ],
        }
    }
}

impl SubAssign for Vec3 {
    fn sub_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            vec: [
                self.vec[0] - other.vec[0],
                self.vec[1] - other.vec[1],
                self.vec[2] - other.vec[2],
            ],
        };
    }
}

impl Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Vec3 {
        Vec3 {
            vec: [-self.vec[0], -self.vec[1], -self.vec[2]],
        }
    }
}

impl Index<usize> for Vec3 {
    type Output = f32;

    fn index(&self, i: usize) -> &f32 {
        &self.vec[i]
    }
}
