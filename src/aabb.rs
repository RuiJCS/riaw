use ray::Ray;
use vec3::Vec3;

#[derive(Clone, Debug)]
pub struct AABB {
    max: Vec3,
    min: Vec3,
}

pub fn surrounding_box(box0: AABB, box1: AABB) -> AABB {
    let small = Vec3::new(
        f32::min(box0.min().x(), box1.min().x()),
        f32::min(box0.min().y(), box1.min().y()),
        f32::min(box0.min().z(), box1.min().z()),
    );
    let big = Vec3::new(
        f32::max(box0.max().x(), box1.max().x()),
        f32::max(box0.max().y(), box1.max().y()),
        f32::max(box0.max().z(), box1.max().z()),
    );
    AABB::new(big, small)
}

impl AABB {
    pub fn new(max: Vec3, min: Vec3) -> AABB {
        AABB { max, min }
    }

    pub fn min(&self) -> Vec3 {
        self.min
    }

    pub fn max(&self) -> Vec3 {
        self.max
    }

    pub fn offset(&mut self, offset_max: &Vec3, offset_min: &Vec3) {
        self.min += *offset_min;
        self.max += *offset_max;
    }

    pub fn hit(&self, r: Ray, mut t_min: f32, mut t_max: f32) -> bool {
        let mut res: bool = true;
        // println!("bounding_box {:?} {:?}", self.max(), self.min());
        for i in 0..3 {
            let inv_d = 1.0 / r.direction()[i];
            let t0 = (self.min()[i] - r.origin()[i]) * inv_d;
            let t1 = (self.max()[i] - r.origin()[i]) * inv_d;
            let (t0, t1) = if inv_d < 0.0 { (t1, t0) } else { (t0, t1) };
            t_min = t_min.max(t0);
            t_max = t_max.min(t1);
            if t_max <= t_min {
                res = false;
            }
        }
        // println!("{}", res);
        res
    }
}
