use aabb::{surrounding_box, AABB};
use hit::HitRecord;
use hit::Hitable;
use material::Lambertian;
use material::Material;
use ray::Ray;
use std::f32::consts::PI;
use vec3::dot;
use vec3::Vec3;

fn get_sphere_uv(p: Vec3) -> (f32, f32) {
    let phi = p.z().atan2(p.x());
    let theta = p.y().asin();
    let u = 1f32 - (phi + PI) / (2f32 * PI);
    let v = (theta + PI / 2f32) / PI;
    (u, v)
}

#[derive(Clone)]
pub struct MovingSphere {
    center0: Vec3,
    center1: Vec3,
    radius: f32,
    mat: Box<dyn Material>,
    time0: f32,
    time1: f32,
}

impl MovingSphere {
    #[allow(dead_code)]
    pub fn new_empty() -> MovingSphere {
        MovingSphere {
            center0: Vec3::new_empty(),
            center1: Vec3::new_empty(),
            radius: 0.,
            mat: Box::new(Lambertian::new()),
            time0: 0.0,
            time1: 0.0,
        }
    }

    pub fn new(
        center0: Vec3,
        center1: Vec3,
        radius: f32,
        m: Box<dyn Material>,
        time0: f32,
        time1: f32,
    ) -> MovingSphere {
        MovingSphere {
            center0: Vec3::new_from_vec3(&center0),
            center1: Vec3::new_from_vec3(&center1),
            radius,
            mat: m.box_clone(),
            time0,
            time1,
        }
    }

    pub fn center(&self, time: f32) -> Vec3 {
        self.center0
            + (self.center1 - self.center0) * ((time - self.time0) / (self.time1 - self.time0))
    }
}

impl Hitable for MovingSphere {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let oc = r.origin() - self.center(r.time);
        let a = dot(&r.direction(), &r.direction());
        let b = dot(&oc, &r.direction());
        let c = dot(&oc, &oc) - self.radius * self.radius;
        let discriminant = b * b - a * c;
        if discriminant > 0. {
            let time: f32 = (-b - discriminant.sqrt()) / a;
            if time < t_max && time > t_min {
                let mut rec = HitRecord::new_empty();
                rec.t = time;
                rec.p = r.point_at_parameter(rec.clone().t);
                rec.normal = (rec.p - self.center(r.time)) / self.radius;
                rec.mat = self.mat.box_clone();
                return Some(rec);
            }

            let time: f32 = (-b + discriminant.sqrt()) / (a);
            if time < t_max && time > t_min {
                let mut rec = HitRecord::new_empty();
                rec.t = time;
                rec.p = r.point_at_parameter(rec.clone().t);
                rec.normal = (rec.p - self.center(r.time)) / self.radius;
                rec.mat = self.mat.box_clone();
                return Some(rec);
            }
        }
        None
    }

    fn bounding_box(&self, t0: f32, t1: f32) -> Option<AABB> {
        let boxe0 = AABB::new(
            self.center(t0) + Vec3::new(self.radius, self.radius, self.radius),
            self.center(t0) - Vec3::new(self.radius, self.radius, self.radius),
        );
        let boxe1 = AABB::new(
            self.center(t1) + Vec3::new(self.radius, self.radius, self.radius),
            self.center(t1) - Vec3::new(self.radius, self.radius, self.radius),
        );
        let boxe = surrounding_box(boxe0, boxe1);
        Some(boxe)
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(self.clone())
    }
}

#[derive(Clone)]
pub struct Sphere {
    center: Vec3,
    radius: f32,
    mat: Box<dyn Material>,
}

impl Sphere {
    #[allow(dead_code)]
    pub fn new_empty() -> Sphere {
        Sphere {
            center: Vec3::new_empty(),
            radius: 0.,
            mat: Box::new(Lambertian::new()),
        }
    }

    pub fn new(center: Vec3, radius: f32, m: Box<dyn Material>) -> Sphere {
        // println!("new sphere");
        Sphere {
            center: Vec3::new_from_vec3(&center),
            radius,
            mat: m.box_clone(),
        }
    }
}

impl Hitable for Sphere {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let oc = r.origin() - self.center;
        let a = dot(&r.direction(), &r.direction());
        let b = dot(&oc, &r.direction());
        let c = dot(&oc, &oc) - self.radius * self.radius;
        let discriminant = b * b - a * c;
        // println!("YOLO");
        if discriminant > 0. {
            let time: f32 = (-b - discriminant.sqrt()) / a;
            let mut rec = HitRecord::new_empty();
            // println!("{} ", discriminant);
            if time < t_max && time > t_min {
                rec.t = time;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p - self.center) / self.radius;
                rec.mat = self.mat.box_clone();
                let temp = get_sphere_uv((rec.p - self.center) / self.radius);
                rec.u = temp.0;
                rec.v = temp.1;
                return Some(rec);
            }

            let time = (-b + discriminant.sqrt()) / (a);
            if time < t_max && time > t_min {
                rec.t = time;
                rec.p = r.point_at_parameter(rec.t);
                rec.normal = (rec.p - self.center) / self.radius;
                rec.mat = self.mat.box_clone();
                let temp = get_sphere_uv((rec.p - self.center) / self.radius);
                rec.u = temp.0;
                rec.v = temp.1;
                return Some(rec);
            }
        }
        None
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<AABB> {
        let boxe = AABB::new(
            self.center + Vec3::new(self.radius, self.radius, self.radius),
            self.center - Vec3::new(self.radius, self.radius, self.radius),
        );
        Some(boxe)
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(self.clone())
    }
}
