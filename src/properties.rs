use serde::{Deserialize, Serialize};
use serde_yaml::{from_str, Value};
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use vec3::Vec3;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Properties {
    pub width: u32,
    pub height: u32,
    pub samples: u32,
    pub max_depth: u32,
    pub scene: String,
    pub scene_file: String,
    pub vfov: f32,
    pub aperture: f32,
    pub look_from: Vec3,
    pub look_at: Vec3,
    pub vup: Vec3,
    pub time0: f32,
    pub time1: f32,
    pub output_name: String,
}

fn read_file(file_name: &str) -> String {
    let mut contents = String::new();
    if let Ok(file) = File::open(file_name) {
        let mut buf_reader = BufReader::new(file);
        buf_reader.read_to_string(&mut contents).unwrap();
    } else {
        contents = "scene:\'\'".to_owned() + &read_file("properties/default.yaml");
    }
    contents
}

fn read_scene_name(file_string: &str) -> String {
    let deserialized_properties: Value = from_str::<serde_yaml::Value>(file_string).unwrap();
    let res: String;
    if let Some(scene) = deserialized_properties.get("scene_file") {
        res = scene.as_str().unwrap().to_string();
    } else {
        res = String::new();
    }
    res
}

impl Properties {
    pub fn new_from_file(file: &str) -> Self {
        let mut contents = read_file(file);
        let scene = read_scene_name(&contents.as_str());
        let file_name = "properties/".to_owned() + &scene;
        contents = contents + "\n" + &read_file(&file_name).as_str();
        let deserialized_properties: Properties = serde_yaml::from_str(&contents).unwrap();
        deserialized_properties
    }
}
