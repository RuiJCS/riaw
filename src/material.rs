use hit::HitRecord;
use ray::Ray;
use texture::{Color, Texture};
use vec3::{dot, reflect, refract, schlick, Vec3};

use rand::prelude::*;

pub trait Material {
    fn scatter(
        &self,
        r: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool;

    fn emitted(&self, u: f32, v: f32, p: &Vec3) -> Vec3;

    fn box_clone(&self) -> Box<dyn Material>;
}

impl Clone for Box<dyn Material> {
    fn clone(&self) -> Box<dyn Material> {
        self.box_clone()
    }
}

#[derive(Clone)]
pub struct Lambertian {
    albedo: Box<dyn Texture>,
}
#[derive(Clone)]
pub struct Metal {
    albedo: Box<dyn Texture>,
    fuz: f32,
}

#[derive(Copy, Clone)]
pub struct Dielectric {
    ref_index: f32,
}

#[derive(Clone)]
pub struct DiffuseLight {
    emitter: Box<dyn Texture>,
}

#[derive(Clone)]
pub struct Isotropic {
    albedo: Box<dyn Texture>,
}

impl Isotropic {
    pub fn new(albedo: Box<dyn Texture>) -> Self {
        Self { albedo }
    }
}

impl Material for Isotropic {
    fn scatter(
        &self,
        r: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        *scattered = Ray::new(rec.p(), random_in_unit_sphere(), r.time);
        *attenuation = Vec3::new_from_vec3(&self.albedo.value(rec.u, rec.v, &rec.p()));
        true
    }
    fn emitted(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        Vec3::new(0., 0., 0.)
    }
    fn box_clone(&self) -> Box<dyn Material> {
        Box::new(self.clone())
    }
}

fn random_in_unit_sphere() -> Vec3 {
    let mut res: Vec3;
    let mut rng = thread_rng();
    loop {
        res = (Vec3::new(
            rng.gen_range(0f32 .. 1f32),
            rng.gen_range(0f32 .. 1f32),
            rng.gen_range(0f32 .. 1f32),
        ) * 2.)
            - Vec3::new(1., 1., 1.);
        if dot(&res, &res) >= 1. {
            return res;
        }
    }
}

impl Lambertian {
    pub fn new() -> Self {
        Lambertian {
            albedo: Box::new(Color::new()),
        }
    }

    pub fn new_from_vec(v: Vec3) -> Self {
        Lambertian {
            albedo: Box::new(Color::new_from_vec3(&v)),
        }
    }

    pub fn new_with_texture(t: Box<dyn Texture>) -> Self {
        Lambertian { albedo: t }
    }
}

impl Material for Lambertian {
    fn scatter(
        &self,
        r: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        let target = rec.normal() + rec.p() + random_in_unit_sphere();
        *scattered = Ray::new(rec.p(), target - rec.p(), r.time);
        *attenuation = Vec3::new_from_vec3(&self.albedo.value(rec.u, rec.v, &rec.p()));
        true
    }

    fn box_clone(&self) -> Box<dyn Material> {
        Box::new(self.clone())
    }

    fn emitted(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        Vec3::new(0.0, 0.0, 0.0)
    }
}

impl Metal {
    #[allow(dead_code)]
    pub fn new() -> Self {
        Metal {
            albedo: Box::new(Color::new()),
            fuz: 0.001,
        }
    }

    pub fn new_from_vec(v: Vec3, f: f32) -> Self {
        let mut fuz = f;
        if fuz > 1. {
            fuz = 1.;
        }
        Metal {
            albedo: Box::new(Color::new_from_vec3(&v)),
            fuz,
        }
    }
}

impl Material for Metal {
    fn scatter(
        &self,
        r: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        let reflected = reflect(&r.direction(), &rec.normal());
        *scattered = Ray::new(
            rec.p(),
            reflected + random_in_unit_sphere() * self.fuz,
            r.time,
        );
        *attenuation = Vec3::new_from_vec3(&self.albedo.value(0.0, 0.0, &Vec3::new_empty()));
        dot(&scattered.direction(), &rec.normal()) > 0.
    }

    fn emitted(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        Vec3::new(0.0, 0.0, 0.0)
    }

    fn box_clone(&self) -> Box<dyn Material> {
        Box::new(self.clone())
    }
}

impl Dielectric {
    #[allow(dead_code)]
    pub fn new_empty() -> Dielectric {
        Dielectric { ref_index: 0. }
    }

    pub fn new(r: f32) -> Dielectric {
        Dielectric { ref_index: r }
    }
}

impl Material for Dielectric {
    fn scatter(
        &self,
        r: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        let res: bool;
        let outward_normal: Vec3;
        let reflected = reflect(&r.direction(), &rec.normal());
        let ni_over_nt: f32;
        let mut refracted = Vec3::new_empty();
        let reflect_prob: f32;
        let cosine: f32;
        *attenuation = Vec3::new(1., 1., 1.);
        if dot(&r.direction(), &rec.normal()) > 0. {
            outward_normal = rec.normal() * -1.;
            ni_over_nt = self.ref_index;
            cosine = self.ref_index * dot(&r.direction(), &rec.normal()) / r.direction().length();
        } else {
            outward_normal = rec.normal();
            ni_over_nt = 1. / self.ref_index;
            cosine = -dot(&r.direction(), &rec.normal()) / r.direction().length();
        }

        if refract(&r.direction(), &outward_normal, ni_over_nt, &mut refracted) {
            reflect_prob = schlick(cosine, self.ref_index);
            res = true;
        } else {
            reflect_prob = 1.;
            res = true;
        }

        let mut rng = thread_rng();
        if rng.gen_range(0f32 .. 1f32) < reflect_prob {
            *scattered = Ray::new(rec.p(), reflected.clone(), r.time);
        } else {
            *scattered = Ray::new(rec.p(), refracted.clone(), r.time);
        }
        res
    }

    fn emitted(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        Vec3::new(0.0, 0.0, 0.0)
    }

    fn box_clone(&self) -> Box<dyn Material> {
        Box::new(self.clone())
    }
}

impl DiffuseLight {
    pub fn new() -> Self {
        DiffuseLight {
            emitter: Box::new(Color::new()),
        }
    }

    pub fn new_from_vec(v: &Vec3) -> Self {
        DiffuseLight {
            emitter: Box::new(Color::new_from_vec3(v)),
        }
    }

    pub fn new_with_texture(t: Box<dyn Texture>) -> Self {
        DiffuseLight { emitter: t }
    }
}

impl Material for DiffuseLight {
    fn scatter(
        &self,
        r: &Ray,
        rec: &HitRecord,
        attenuation: &mut Vec3,
        scattered: &mut Ray,
    ) -> bool {
        let target = rec.normal() + rec.p() + random_in_unit_sphere();
        *scattered = Ray::new(rec.p(), target - rec.p(), r.time);
        *attenuation = Vec3::new_from_vec3(&self.emitter.value(rec.u, rec.v, &rec.p()));
        false
    }

    fn box_clone(&self) -> Box<dyn Material> {
        Box::new(self.clone())
    }

    fn emitted(&self, u: f32, v: f32, p: &Vec3) -> Vec3 {
        self.emitter.value(u, v, p)
    }
}
