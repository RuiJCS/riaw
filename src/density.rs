use crate::{
    hit::{HitRecord, Hitable},
    material::{Isotropic, Material},
    random_float,
    texture::Texture,
    vec3::Vec3,
};
use std::f32;

#[derive(Clone)]
pub struct ConstantMedium {
    neg_inv_density: f32,
    boundary: Box<dyn Hitable>,
    phase_function: Box<dyn Material>,
}

impl ConstantMedium {
    pub fn new(
        density: f32,
        boundary: Box<dyn Hitable>,
        phase_function_texture: Box<dyn Texture>,
    ) -> Self {
        let neg_inv_density = -1. / density;
        let phase_function = Box::new(Isotropic::new(phase_function_texture));
        Self {
            neg_inv_density,
            boundary,
            phase_function,
        }
    }
}

impl Hitable for ConstantMedium {
    fn hit(&self, r: crate::ray::Ray, t_min: f32, t_max: f32) -> Option<crate::hit::HitRecord> {
        let rec1 = self.boundary.hit(r, -f32::INFINITY, f32::INFINITY);
        let rec2 = if rec1.is_some() {
            self.boundary
                .hit(r, rec1.clone().unwrap().t + 0.0001, f32::INFINITY)
        } else {
            None
        };

        if rec2.is_none() {
            return None;
        }
        let (mut rec1, mut rec2) = (rec1.unwrap(), rec2.unwrap());

        if rec1.t < t_min {
            rec1.t = t_min;
        }
        if rec2.t > t_max {
            rec2.t = t_max;
        }

        if rec1.t >= rec2.t {
            return None;
        }

        if rec1.t < 0. {
            rec1.t = 0.;
        }

        let ray_length = r.direction().length();
        let distance_inside_boundary = (rec2.t - rec1.t) * ray_length;
        let hit_distance = self.neg_inv_density * random_float(0., 1.).log(10.);

        if hit_distance > distance_inside_boundary {
            return None;
        }

        let mut rec = HitRecord::new_empty();
        rec.t = rec1.t + hit_distance / ray_length;
        rec.p = r.point_at_parameter(rec.t);

        rec.normal = Vec3::new(1., 0., 0.); // arbitrary
        rec.front_face = true; // also arbitrary
        rec.mat = self.phase_function.clone();
        Some(rec)
    }
    fn bounding_box(&self, t0: f32, t1: f32) -> Option<crate::aabb::AABB> {
        self.boundary.bounding_box(t0, t1)
    }
    fn box_clone(&self) -> Box<dyn Hitable> {
        Box::new(self.clone())
    }
}
