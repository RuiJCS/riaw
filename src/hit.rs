use crate::vec3::dot;
use aabb::{surrounding_box, AABB};
use material::{Lambertian, Material};
use ray::Ray;
use std::fmt;
use std::fmt::Debug;
use std::fmt::Formatter;
use vec3::Vec3;

pub trait Hitable {
    fn hit(&self, _r: Ray, _t_min: f32, _t_max: f32) -> Option<HitRecord> {
        panic!("Not implemented");
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<AABB> {
        panic!("Not implemented!");
    }

    fn box_clone(&self) -> Box<dyn Hitable>;
}

#[derive(Clone)]
pub struct HitRecord {
    pub t: f32,
    pub p: Vec3,
    pub normal: Vec3,
    pub mat: Box<dyn Material>,
    pub u: f32,
    pub v: f32,
    pub front_face: bool,
}

impl Debug for HitRecord {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Point {{ t: {}, p: {:?}, normal:{:?} }}",
            self.t,
            self.p,
            self.normal()
        )
    }
}

#[allow(dead_code)]
pub struct HitableList {
    size: u32,
    pub list: Vec<Box<dyn Hitable>>,
}

unsafe impl Send for HitableList {}
unsafe impl Sync for HitableList {}

impl HitRecord {
    pub fn new_empty() -> HitRecord {
        HitRecord {
            t: 0.,
            p: Vec3::new_empty(),
            normal: Vec3::new_empty(),
            mat: Box::new(Lambertian::new()),
            u: 0f32,
            v: 0f32,
            front_face: false,
        }
    }

    pub fn normal(&self) -> Vec3 {
        self.normal.clone()
    }

    pub fn p(&self) -> Vec3 {
        self.p.clone()
    }

    pub fn mat(&self) -> Box<dyn Material> {
        self.mat.clone()
    }

    pub fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
        let front_face = dot(&r.direction(), outward_normal) < 0f32;
        self.normal = if front_face {
            *outward_normal
        } else {
            -*outward_normal
        };
    }
}

impl HitableList {
    #[allow(dead_code)]
    pub fn new_empty() -> HitableList {
        HitableList {
            size: 0,
            list: Vec::new(),
        }
    }

    pub fn new(list: Vec<Box<dyn Hitable>>) -> HitableList {
        HitableList {
            size: list.len() as u32,
            list,
        }
    }
}

impl Hitable for HitableList {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let mut closest_so_far = t_max;
        let mut rec: Option<HitRecord> = None;
        // println!("HL");
        for h in self.list.as_slice().iter() {
            let temp_rec = h.hit(r, t_min, closest_so_far);
            rec = match temp_rec {
                None => rec,
                Some(hit) => {
                    closest_so_far = hit.t;
                    Some(hit)
                }
            };
        }
        rec
    }

    fn bounding_box(&self, t0: f32, t1: f32) -> Option<AABB> {
        if self.size < 1 {
            return Option::None;
        }
        let mut res: Option<AABB> = self.list[0].bounding_box(t0, t1);
        for h in self.list.iter() {
            let temp_box = h.bounding_box(t0, t1);
            res = match temp_box {
                None => res,
                Some(x) => Some(surrounding_box(res.unwrap(), x)),
            };
        }
        res
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(HitableList::new(self.list.clone().to_vec()))
    }
}

impl Clone for Box<dyn Hitable> {
    fn clone(&self) -> Box<dyn Hitable> {
        self.box_clone()
    }
}
