use crate::{
    aabb::AABB,
    hit::{HitRecord, HitableList},
    vec3::Vec3,
};
use hit::Hitable;
use material::Material;
use Ray;

#[derive(Clone)]
pub struct SquareXY {
    x0: f32,
    x1: f32,
    y0: f32,
    y1: f32,
    k: f32,
    mat: Box<dyn Material>,
    flip: i8,
}

#[derive(Clone)]
pub struct SquareXZ {
    x0: f32,
    x1: f32,
    z0: f32,
    z1: f32,
    k: f32,
    mat: Box<dyn Material>,
    flip: i8,
}

#[derive(Clone)]
pub struct SquareYZ {
    y0: f32,
    y1: f32,
    z0: f32,
    z1: f32,
    k: f32,
    mat: Box<dyn Material>,
    flip: i8,
}

pub struct CornellBox {
    box_min: Vec3,
    box_max: Vec3,
    sides: HitableList,
}

impl CornellBox {
    pub fn new(p0: &Vec3, p1: &Vec3, mat: Box<dyn Material>) -> Self {
        let box_min = *p0;
        let box_max = *p1;
        let mut list: Vec<Box<dyn Hitable>> = Vec::default();
        list.push(Box::new(SquareXY::new(
            p0.x(),
            p1.x(),
            p0.y(),
            p1.y(),
            p1.z(),
            mat.clone(),
            1,
        )));
        list.push(Box::new(SquareXY::new(
            p0.x(),
            p1.x(),
            p0.y(),
            p1.y(),
            p0.z(),
            mat.clone(),
            -1,
        )));

        list.push(Box::new(SquareXZ::new(
            p0.x(),
            p1.x(),
            p0.z(),
            p1.z(),
            p1.y(),
            mat.clone(),
            1,
        )));
        list.push(Box::new(SquareXZ::new(
            p0.x(),
            p1.x(),
            p0.z(),
            p1.z(),
            p0.y(),
            mat.clone(),
            -1,
        )));

        list.push(Box::new(SquareYZ::new(
            p0.y(),
            p1.y(),
            p0.z(),
            p1.z(),
            p1.x(),
            mat.clone(),
            1,
        )));
        list.push(Box::new(SquareYZ::new(
            p0.y(),
            p1.y(),
            p0.z(),
            p1.z(),
            p0.x(),
            mat.clone(),
            -1,
        )));

        let sides: HitableList = HitableList::new(list);
        Self {
            box_min,
            box_max,
            sides,
        }
    }
}

impl Clone for CornellBox {
    fn clone(&self) -> Self {
        Self {
            box_min: self.box_min.clone(),
            box_max: self.box_max.clone(),
            sides: HitableList::new(self.sides.list.clone()),
        }
    }
}

impl Hitable for CornellBox {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        self.sides.hit(r, t_min, t_max)
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<AABB> {
        let boxe = AABB::new(self.box_max, self.box_min);
        Some(boxe)
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(self.clone())
    }
}

impl SquareXY {
    pub fn new(
        x0: f32,
        x1: f32,
        y0: f32,
        y1: f32,
        k: f32,
        mat: Box<dyn Material>,
        flip: i8,
    ) -> Self {
        Self {
            x0,
            x1,
            y0,
            y1,
            k,
            mat,
            flip,
        }
    }
}

impl Hitable for SquareXY {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<crate::hit::HitRecord> {
        let origin = &r.origin();
        let direction = &r.direction();
        let t = (self.k - origin.z()) / direction.z();
        if t < t_min || t > t_max {
            return None;
        }
        let x = origin.x() + t * direction.x();
        let y = origin.y() + t * direction.y();
        if x < self.x0 || x > self.x1 || y < self.y0 || y > self.y1 {
            return None;
        }
        let mut rec = HitRecord::new_empty();
        rec.u = (x - self.x0) / (self.x1 - self.x0);
        rec.v = (y - self.y0) / (self.y1 - self.y0);
        rec.t = t;
        let outward_normal = Vec3::new(0., 0., 1.0);
        rec.set_face_normal(&r, &(outward_normal * self.flip));
        rec.p = r.point_at_parameter(rec.t);
        rec.mat = self.mat.box_clone();
        return Some(rec);
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<crate::aabb::AABB> {
        let boxe = AABB::new(
            Vec3::new(self.x1, self.y1, self.k + 0.0001),
            Vec3::new(self.x0, self.y0, self.k - 0.0001),
        );
        Some(boxe)
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(self.clone())
    }
}

impl SquareXZ {
    pub fn new(
        x0: f32,
        x1: f32,
        z0: f32,
        z1: f32,
        k: f32,
        mat: Box<dyn Material>,
        flip: i8,
    ) -> Self {
        Self {
            x0,
            x1,
            z0,
            z1,
            k,
            mat,
            flip,
        }
    }
}

impl Hitable for SquareXZ {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<crate::hit::HitRecord> {
        let origin = &r.origin();
        let direction = &r.direction();
        let t = (self.k - origin.y()) / direction.y();
        if t < t_min || t > t_max {
            return None;
        }
        let x = origin.x() + t * direction.x();
        let y = origin.z() + t * direction.z();
        if x < self.x0 || x > self.x1 || y < self.z0 || y > self.z1 {
            return None;
        }
        let mut rec = HitRecord::new_empty();
        rec.u = (x - self.x0) / (self.x1 - self.x0);
        rec.v = (y - self.z0) / (self.z1 - self.z0);
        rec.t = t;
        let outward_normal = Vec3::new(0., 1., 0.);
        rec.set_face_normal(&r, &(outward_normal * self.flip));
        rec.p = r.point_at_parameter(rec.t);
        rec.mat = self.mat.box_clone();
        return Some(rec);
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<crate::aabb::AABB> {
        let boxe = AABB::new(
            Vec3::new(self.x1, self.k + 0.0001, self.z1),
            Vec3::new(self.x0, self.k - 0.0001, self.z0),
        );
        Some(boxe)
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(self.clone())
    }
}

impl SquareYZ {
    pub fn new(
        y0: f32,
        y1: f32,
        z0: f32,
        z1: f32,
        k: f32,
        mat: Box<dyn Material>,
        flip: i8,
    ) -> Self {
        Self {
            y0,
            y1,
            z0,
            z1,
            k,
            mat,
            flip,
        }
    }
}

impl Hitable for SquareYZ {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<crate::hit::HitRecord> {
        let origin = &r.origin();
        let direction = &r.direction();
        let t = (self.k - origin.x()) / direction.x();
        if t < t_min || t > t_max {
            return None;
        }
        let y = origin.y() + t * direction.y();
        let z = origin.z() + t * direction.z();
        if y < self.y0 || y > self.y1 || z < self.z0 || z > self.z1 {
            return None;
        }
        let mut rec = HitRecord::new_empty();
        rec.u = (y - self.y0) / (self.y1 - self.y0);
        rec.v = (z - self.z0) / (self.z1 - self.z0);
        rec.t = t;
        let outward_normal = Vec3::new(1., 0., 0.);
        rec.set_face_normal(&r, &(outward_normal * self.flip));
        rec.p = r.point_at_parameter(rec.t);
        rec.mat = self.mat.box_clone();
        return Some(rec);
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<crate::aabb::AABB> {
        let boxe = AABB::new(
            Vec3::new(self.k + 0.0001, self.y1, self.z1),
            Vec3::new(self.k - 0.0001, self.y0, self.z0),
        );
        Some(boxe)
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(self.clone())
    }
}
