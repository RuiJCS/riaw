use f32::consts::PI;
use rand::prelude::*;
use ray::Ray;
use vec3::{cross, dot, Vec3};

#[derive(Clone, Copy)]
#[allow(dead_code)]
pub struct Camera {
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    origin: Vec3,
    u: Vec3,
    v: Vec3,
    w: Vec3,
    lens_radius: f32,
    time0: f32,
    time1: f32,
}

impl Camera {
    pub fn new_generic(
        vfov: f32,
        aspect: f32,
        aperture: f32,
        focus_dist: f32,
        lookfrom: &Vec3,
        lookat: &Vec3,
        vup: &Vec3,
        t0: f32,
        t1: f32,
    ) -> Camera {
        let u: Vec3;
        let v: Vec3;
        let w: Vec3;
        w = (*lookfrom - *lookat).unit_vector();
        u = cross(vup, &w).unit_vector();
        v = cross(&w, &u);
        let theta = vfov * PI / 180.;
        let half_height = (theta / 2.).tan();
        let half_width = aspect * half_height;
        Camera {
            origin: Vec3::new_from_vec3(&lookfrom),
            lower_left_corner: lookfrom.clone()
                - u * focus_dist * half_width
                - v * focus_dist * half_height
                - w * focus_dist,
            horizontal: u * focus_dist * half_width * 2.,
            vertical: v * focus_dist * half_height * 2.,
            u,
            v,
            w,
            lens_radius: aperture / 2.,
            time0: t0,
            time1: t1,
        }
    }

    pub fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd: Vec3 = random_in_unit_disk() * self.lens_radius;
        let offset = self.u * rd.x() + self.v * rd.y();
        let direction =
            self.lower_left_corner + self.horizontal * s + self.vertical * t - self.origin - offset;
        let mut rng = thread_rng();
        let time = self.time0 + rng.gen::<f32>() * (self.time1 - self.time0);
        Ray::new(self.origin + offset, direction, time)
    }
}

fn random_in_unit_disk() -> Vec3 {
    let mut p: Vec3;
    let mut rng = thread_rng();
    loop {
        p = (Vec3::new(rng.gen::<f32>(), rng.gen::<f32>(), 0.) * 2.) - Vec3::new(1., 1., 0.);
        if dot(&p, &p) < 1. {
            return p;
        }
    }
}
