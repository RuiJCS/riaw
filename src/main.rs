use hit::Hitable;
use std::f32;
use std::time::SystemTime;

mod vec3;
use vec3::Vec3;

mod ray;
use ray::Ray;

mod hit;

mod sphere;

mod camera;
use camera::Camera;

mod material;

mod aabb;
mod bvh;
mod properties;
mod texture;
use properties::Properties;
mod noise;
mod scenes;
use scenes::scene;
mod density;
mod square;
mod transformation;

extern crate rand;
use rand::prelude::{thread_rng, Rng};

extern crate rayon;
use rayon::prelude::*;

extern crate image;

//libraries to read YAML files
extern crate serde;
extern crate serde_yaml;

const BACKGROUND_COLOR: Vec3 = Vec3::new_const(0.0, 0.0, 0.0);

fn random_float(min: f32, max: f32) -> f32 {
    let mut rng = thread_rng();
    return rng.gen_range(min.. max);
}

fn read_properties() -> Properties {
    Properties::new_from_file("properties/properties.yaml")
}

fn colour(r: Ray, world: &dyn Hitable, depth: u32, properties: &Properties) -> Vec3 {
    let mut res = Vec3::new_empty();
    let mut scattered: Ray = Ray::new_empty();
    let mut attenuation: Vec3 = Vec3::new_empty();
    if depth <= properties.max_depth {
        // println!("hit world");
        let hit = world.hit(r, 0.001, f32::MAX);
        res = match hit {
            None => {
                // Background
                // let unit_direction = r.direction().unit_vector();
                // let t = 0.5 * (unit_direction.y() + 1.);
                // Vec3::new(1., 1., 1.) * (1. - t) + Vec3::new(0.5, 0.7, 1.0) * t
                BACKGROUND_COLOR
            }
            Some(hit) => {
                let color = hit.mat.emitted(hit.u, hit.v, &hit.p);
                if hit
                    .mat()
                    .scatter(&r, &hit, &mut attenuation, &mut scattered)
                {
                    color + attenuation * colour(scattered, world, depth + 1, properties)
                } else {
                    color
                }
            }
        };
    }
    res
}

fn cast_ray(
    i: u32,
    y: u32,
    cam: &Camera,
    world: &dyn Hitable,
    p: &mut [u8; 3],
    properties: &Properties,
) {
    let j = properties.height - 1 - y / properties.width;
    let mut color = Vec3::new_empty();
    let mut rng = thread_rng();
    for _ in 0..properties.samples {
        let u = (i as f32 + rng.gen::<f32>()) / properties.width as f32;
        let v = (j as f32 + rng.gen::<f32>()) / properties.height as f32;
        let r = cam.get_ray(u, v);
        color += colour(r, world, 0, properties);
    }
    color /= properties.samples;
    color.sqrt();
    color.clamp(0f32, 0.99);
    *p = [
        (256. * color[0]) as u8,
        (256. * color[1]) as u8,
        (256. * color[2]) as u8,
    ];
}

fn main() {
    let now = SystemTime::now();
    let properties = read_properties();
    let mut image = image::RgbImage::new(properties.width, properties.height);
    // let world = random_scene();
    // let world = two_spheres_scene();
    let world = scene(&properties.scene.as_str());
    let focus_dist = (properties.look_from - properties.look_at).length();

    let cam: Camera = Camera::new_generic(
        properties.vfov,
        (properties.width / properties.height) as f32,
        properties.aperture,
        focus_dist,
        &properties.look_from,
        &properties.look_at,
        &properties.vup,
        properties.time0,
        properties.time1,
    );

    let mut aux_image: Vec<[u8; 3]> =
        vec![[0, 0, 0]; properties.width as usize * properties.height as usize];

    print!("Done with preparations ");
    match now.elapsed() {
        Ok(elapsed) => {
            println!("took {} milliseconds.", elapsed.as_millis());
        }
        Err(e) => {
            println!("error: {:?}", e);
        }
    }
    
    let now = SystemTime::now();

    let mut is: Vec<u32> = Vec::new();
    let ys: Vec<u32> = (0..properties.height * properties.width).collect();
    for _i in 0..properties.height {
        let mut aux_: Vec<u32> = (0..properties.width).collect();
        is.append(&mut aux_);
    }

    is.as_slice()
        .par_iter()
        .zip(ys.as_slice())
        .zip(aux_image.as_mut_slice())
        .for_each(|((i, y), p)| cast_ray(*i, *y, &cam, &world, p, &properties));

    let mut aux: Vec<u8> = Vec::new();
    for v in aux_image {
        aux.push(v[0]);
        aux.push(v[1]);
        aux.push(v[2]);
    }
    image.copy_from_slice(aux.as_slice());
    /* for y in 0..properties.height {
        for i in 0..properties.width {
            let j = properties.height - 1 - y;
            let mut color = Vec3::new_empty();
            let mut rng = thread_rng();
            for _ in 0..properties.samples {
                let u = (i as f32 + rng.gen::<f32>()) / properties.width as f32;
                let v = (j as f32 + rng.gen::<f32>()) / properties.height as f32;
                // let aux = Vec3::new(i as f32 / properties.width as f32,j as f32 / properties.height as f32,0.);
                let r = cam.get_ray(u, v);
                color += colour(r, &world, 0);
            }
            color /= properties.samples;
            color.sqrt();

            let pixel = image::Rgb([
                (255.99 * color[0]) as u8,
                (255.99 * color[1]) as u8,
                (255.99 * color[2]) as u8,
            ]);

            *image.get_pixel_mut(i, y) = pixel;
        }
    }*/

    image.save(properties.output_name).unwrap();
    match now.elapsed() {
        Ok(elapsed) => {
            println!("Took {} milliseconds.", elapsed.as_millis());
        }
        Err(e) => {
            println!("Error: {:?}", e);
        }
    }
}
