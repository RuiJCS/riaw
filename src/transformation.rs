use crate::{
    aabb,
    hit::{self, Hitable},
    ray::Ray,
    vec3::Vec3,
};
use aabb::AABB;
use std::f32;

#[derive(Clone)]
pub struct Translate {
    offset: Vec3,
    original: Box<dyn Hitable>,
}

#[derive(Clone)]
pub struct RotateY {
    original: Box<dyn Hitable>,
    sin_theta: f32,
    cos_theta: f32,
    bbox: AABB,
}

impl RotateY {
    pub fn new(original: Box<dyn Hitable>, angle: f32) -> Self {
        let bbox = original.bounding_box(0., 1.);
        if bbox.is_some() {
            let bbox = bbox.unwrap();
            let radians = angle.to_radians();
            let sin_theta = radians.sin();
            let cos_theta = radians.cos();

            let mut min = Vec3::new(f32::INFINITY, f32::INFINITY, f32::INFINITY);
            let mut max = Vec3::new(-f32::INFINITY, -f32::INFINITY, -f32::INFINITY);

            for i in 0..2 {
                for j in 0..2 {
                    for k in 0..2 {
                        let x = i as f32 * bbox.max().x() + (1 - i) as f32 * bbox.min().x();
                        let y = j as f32 * bbox.max().y() + (1 - j) as f32 * bbox.min().y();
                        let z = k as f32 * bbox.max().z() + (1 - k) as f32 * bbox.min().z();

                        let newx = cos_theta * x + sin_theta * z;
                        let newz = -sin_theta * x + cos_theta * z;

                        min = Vec3::new(min[0].min(newx), min[1].min(y), min[2].min(newz));
                        max = Vec3::new(max[0].max(newx), max[1].max(y), max[2].max(newz));
                    }
                }
            }
            let bbox = AABB::new(max, min);
            Self {
                original,
                sin_theta,
                cos_theta,
                bbox,
            }
        } else {
            panic!("Couldn't find bounding box ");
        }
    }
}

impl Hitable for RotateY {
    fn box_clone(&self) -> Box<dyn Hitable> {
        Box::new(self.clone())
    }
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<hit::HitRecord> {
        let origin = Vec3::new(
            self.cos_theta * r.origin()[0] - self.sin_theta * r.origin()[2],
            r.origin()[1],
            self.sin_theta * r.origin()[0] + self.cos_theta * r.origin()[2],
        );

        let direction = Vec3::new(
            self.cos_theta * r.direction()[0] - self.sin_theta * r.direction()[2],
            r.direction()[1],
            self.sin_theta * r.direction()[0] + self.cos_theta * r.direction()[2],
        );

        let rotated_r = Ray::new(origin, direction, r.time);
        match self.original.hit(rotated_r, t_min, t_max) {
            Some(mut hit) => {
                hit.p = Vec3::new(
                    self.cos_theta * hit.p[0] + self.sin_theta * hit.p[2],
                    hit.p[1],
                    -self.sin_theta * hit.p[0] + self.cos_theta * hit.p[2],
                );
                let normal = Vec3::new(
                    self.cos_theta * hit.normal[0] + self.sin_theta * hit.normal[2],
                    hit.normal[1],
                    -self.sin_theta * hit.normal[0] + self.cos_theta * hit.normal[2],
                );

                hit.set_face_normal(&rotated_r, &normal);
                Some(hit)
            }
            None => None,
        }
    }
    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<AABB> {
        Some(self.bbox.clone())
    }
}

impl Translate {
    pub fn new(original: Box<dyn Hitable>, offset: Vec3) -> Self {
        Self { offset, original }
    }
}

impl Hitable for Translate {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<hit::HitRecord> {
        let moved_r = Ray::new(r.origin() - self.offset, r.direction(), r.time);
        match self.original.hit(moved_r, t_min, t_max) {
            Some(mut hit) => {
                hit.p += self.offset;
                hit.set_face_normal(&moved_r, &hit.normal.clone());
                Some(hit)
            }
            None => None,
        }
    }
    fn bounding_box(&self, t0: f32, t1: f32) -> Option<aabb::AABB> {
        match self.original.bounding_box(t0, t1) {
            Some(mut aabb) => {
                aabb.offset(&self.offset, &self.offset);
                Some(aabb)
            }
            None => None,
        }
    }
    fn box_clone(&self) -> Box<dyn Hitable> {
        Box::new(self.clone())
    }
}
