use aabb::surrounding_box;
use aabb::AABB;
use hit::HitRecord;
use hit::Hitable;
use hit::HitableList;
use rand::prelude::{thread_rng, Rng};
use ray::Ray;
use std::cmp::Ordering;
use vec3::Vec3;

pub struct BvhNode {
    left_node: Option<Box<dyn Hitable>>,
    right_node: Option<Box<dyn Hitable>>,
    bounding_box: AABB,
}

unsafe impl Send for BvhNode {}
unsafe impl Sync for BvhNode {}

macro_rules! box_compare {
    ( $i:ident, $a:ident, $b:ident ) => {{
        let box_left = AABB::new(Vec3::new(0., 0., 0.), Vec3::new(0., 0., 0.));
        let box_right = AABB::new(Vec3::new(0., 0., 0.), Vec3::new(0., 0., 0.));
        if $a.bounding_box(0., 0.).is_some() || $b.bounding_box(0., 0.).is_some() {
            // error
            let c = box_left.min().$i() - box_right.min().$i();
            if c < 0. {
                Ordering::Less
            } else if c > 0. {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        } else {
            Ordering::Equal
        }
    }};
}

impl BvhNode {
    pub fn new(hitables: HitableList, time0: f32, time1: f32) -> Self {
        let axis = thread_rng().gen_range(0.. 2);
        let mut list = hitables.list.clone();
        // sorting the tree
        if axis == 0 {
            list.sort_unstable_by(|a, b| box_compare!(x, a, b));
        } else if axis == 1 {
            list.sort_unstable_by(|a, b| box_compare!(y, a, b));
        } else {
            list.sort_unstable_by(|a, b| box_compare!(z, a, b));
        }

        let (left, right): (Option<Box<dyn Hitable>>, Option<Box<dyn Hitable>>) = if list.len() == 1
        {
            (Some(list[0].clone()), None)
        } else if list.len() == 2 {
            (Some(list[0].clone()), Some(list[1].clone()))
        } else {
            let (svf, svl) = list.split_at(list.len() / 2);
            let vl = HitableList::new(svl.to_vec());
            let vr = HitableList::new(svf.to_vec());
            (
                Some(Box::new(BvhNode::new(vl, time0, time1))),
                Some(Box::new(BvhNode::new(vr, time0, time1))),
            )
        };

        let box_left = left.clone().unwrap().bounding_box(time0, time1).unwrap();
        let box_right: AABB;
        let bounding_box: AABB;
        if right.is_some() {
            box_right = right.clone().unwrap().bounding_box(time0, time1).unwrap();
            bounding_box = surrounding_box(box_left, box_right)
        } else {
            // println!("new");
            // box_right = AABB::new(Vec3::new(std::f32::MIN, std::f32::MIN, std::f32::MIN), Vec3::new(std::f32::MAX, std::f32::MAX, std::f32::MAX));
            bounding_box = box_left;
        }

        // println!("{:?}", bounding_box);
        Self {
            left_node: left,
            right_node: right,
            bounding_box: bounding_box,
        }
    }
}

impl Hitable for BvhNode {
    fn hit(&self, r: Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        // println!("hit bvh");
        let res: Option<HitRecord> = match self.bounding_box.hit(r, t_min, t_max) {
            false => None,
            true => {
                let _left_hit: bool;
                let _right_hit: bool;
                let hit_left: Option<HitRecord>;
                let hit_right: Option<HitRecord>;
                if self.left_node.is_some() {
                    hit_left = self.left_node.as_ref().unwrap().hit(r, t_min, t_max);
                } else {
                    hit_left = None;
                }
                if self.right_node.is_some() {
                    hit_right = self.right_node.as_ref().unwrap().hit(r, t_min, t_max);
                } else {
                    hit_right = None;
                }

                if hit_left.is_some() && hit_right.is_some() {
                    if hit_right.clone().unwrap().t > hit_left.clone().unwrap().t {
                        return hit_left;
                    } else {
                        return hit_right;
                    }
                } else if hit_left.is_some() {
                    return hit_left;
                } else if hit_right.is_some() {
                    return hit_right;
                }
                None
            }
        };
        res
    }

    fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<AABB> {
        Some(self.bounding_box.clone())
    }

    fn box_clone(&self) -> Box<(dyn Hitable + 'static)> {
        Box::new(BvhNode {
            left_node: self.left_node.clone(),
            right_node: self.right_node.clone(),
            bounding_box: self.bounding_box.clone(),
        })
    }
}
