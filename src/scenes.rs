use crate::{
    density::ConstantMedium,
    material::DiffuseLight,
    random_float,
    square::{CornellBox, SquareXY, SquareXZ, SquareYZ},
    transformation::{RotateY, Translate},
};
use bvh::BvhNode;
use hit::{Hitable, HitableList};
use material::{Dielectric, Lambertian, Metal};
use rand::prelude::{thread_rng, Rng};
use sphere::{MovingSphere, Sphere};
use std::f32;
use texture::{Checker, Color, MarbleTexture, NoiseTexture, SphereImageTexture};
use vec3::Vec3;

fn two_spheres_scene() -> BvhNode {
    let mut spheres: Vec<Box<dyn Hitable>> = Vec::default();

    let color = Color::new_from_vec3(&Vec3::new(4., 4., 4.));
    let diffuse_light = DiffuseLight::new_from_vec(&color.color);

    spheres.push(Box::new(Sphere::new(
        Vec3::new(13., 2., 10.),
        5.,
        Box::new(diffuse_light),
    )));

    let checker = Checker::new_from_vec3(
        &Color::new_from_vec3(&Vec3::new(0.2, 0.3, 0.1)),
        &Color::new_from_vec3(&Vec3::new(0.9, 0.9, 0.9)),
    );
    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., -10., 0.),
        10.,
        Box::new(Lambertian::new_with_texture(Box::new(checker))),
    )));
    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., 10., 0.),
        10.,
        Box::new(Lambertian::new_with_texture(Box::new(checker))),
    )));

    let list: HitableList = HitableList::new(spheres);
    BvhNode::new(list, 0.0, 1.0)
}

fn random_scene() -> BvhNode {
    let mut spheres: Vec<Box<dyn Hitable>> = Vec::default();

    let checker = Checker::new_from_vec3(
        &Color::new_from_vec3(&Vec3::new(0., 0., 0.)),
        &Color::new_from_vec3(&Vec3::new(1., 1., 1.)),
    );
    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., -1000., 0.),
        1000.,
        Box::new(Lambertian::new_with_texture(Box::new(checker))),
    )));
    for a in -11..11 {
        for b in -11..11 {
            let mut rng = thread_rng();
            let choose_mat: f32 = rng.gen();
            let r1: f32 = rng.gen();
            let r2: f32 = rng.gen();
            let center = Vec3::new(a as f32 + 0.9 * r1, 0.2, b as f32 + 0.9 * r2);
            let sphere: Sphere;
            let moving_sphere: MovingSphere;

            if (center - Vec3::new(4., 0.2, 0.)).length() > 0.9 {
                let r: f32;
                let g: f32;
                let b: f32;
                if choose_mat < 0.8 {
                    // diffuse mat
                    r = rng.gen();
                    g = rng.gen();
                    b = rng.gen();
                    moving_sphere = MovingSphere::new(
                        center,
                        center + Vec3::new(0.0, 0.5 * rng.gen::<f32>(), 0.0),
                        0.2,
                        Box::new(Lambertian::new_from_vec(Vec3::new(
                            r.powf(2.),
                            g.powf(2.),
                            b.powf(2.),
                        ))),
                        0.0,
                        1.0,
                    );
                    spheres.push(Box::new(moving_sphere));
                } else if choose_mat < 0.95 {
                    // metal material
                    r = rng.gen();
                    g = rng.gen();
                    b = rng.gen();
                    let albedo: f32 = rng.gen();
                    sphere = Sphere::new(
                        center,
                        0.2,
                        Box::new(Metal::new_from_vec(
                            Vec3::new(0.5 * (1. - r), 0.5 * (1. - g), 0.5 * (1. - b)),
                            albedo,
                        )),
                    );
                    spheres.push(Box::new(sphere));
                } else {
                    // dielectric material
                    sphere = Sphere::new(center, 0.2, Box::new(Dielectric::new(1.5)));
                    spheres.push(Box::new(sphere));
                }
            }
        }
    }

    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., 1., 0.),
        1.,
        Box::new(Dielectric::new(1.5)),
    )));
    spheres.push(Box::new(Sphere::new(
        Vec3::new(-4., 1., 0.),
        1.,
        Box::new(Lambertian::new_from_vec(Vec3::new(0.4, 0.2, 0.1))),
    )));
    spheres.push(Box::new(Sphere::new(
        Vec3::new(4., 1., 0.),
        1.,
        Box::new(Metal::new_from_vec(Vec3::new(0.7, 0.6, 0.5), 0.)),
    )));

    let list: HitableList = HitableList::new(spheres);
    BvhNode::new(list, 0.0, 1.0)
    // list
}

fn sphere() -> BvhNode {
    let mut spheres: Vec<Box<dyn Hitable>> = Vec::default();

    spheres.push(Box::new(Sphere::new(
        Vec3::new(-4., 1., 0.),
        1.,
        Box::new(Lambertian::new_from_vec(Vec3::new(0.4, 0.2, 0.1))),
    )));

    let list: HitableList = HitableList::new(spheres);
    BvhNode::new(list, 0.0, 1.0)
}

fn two_perlin_spheres() -> BvhNode {
    let mut spheres: Vec<Box<dyn Hitable>> = Vec::default();
    let color = Color::new_from_vec3(&Vec3::new(4., 4., 4.));
    let diffuse_light = DiffuseLight::new_from_vec(&color.color);

    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., 1., 30.),
        20.,
        Box::new(diffuse_light),
    )));

    let perlin_texture = MarbleTexture::new();
    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., -1000., 0.),
        1000.,
        Box::new(Lambertian::new_with_texture(Box::new(perlin_texture))),
    )));
    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., 2., 0.),
        2.,
        Box::new(Lambertian::new_with_texture(Box::new(perlin_texture))),
    )));

    let list: HitableList = HitableList::new(spheres);
    BvhNode::new(list, 0.0, 1.0)
}

fn earth_sphere() -> BvhNode {
    let mut spheres: Vec<Box<dyn Hitable>> = Vec::default();
    let image_texture = SphereImageTexture::new("images/earth.png");

    let color = Color::new_from_vec3(&Vec3::new(4., 4., 4.));
    let diffuse_light = DiffuseLight::new_from_vec(&color.color);

    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., 7., 0.),
        2.,
        Box::new(diffuse_light),
    )));

    spheres.push(Box::new(Sphere::new(
        Vec3::new(-4., 1., 0.),
        1.,
        Box::new(Lambertian::new_with_texture(Box::new(image_texture))),
    )));

    let list: HitableList = HitableList::new(spheres);
    BvhNode::new(list, 0.0, 1.0)
}

fn square_light() -> BvhNode {
    let mut spheres: Vec<Box<dyn Hitable>> = Vec::default();
    let perlin_texture = MarbleTexture::new();

    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., -1000., 0.),
        1000.,
        Box::new(Lambertian::new_with_texture(Box::new(
            perlin_texture.clone(),
        ))),
    )));
    spheres.push(Box::new(Sphere::new(
        Vec3::new(0., 2., 0.),
        2.,
        Box::new(Lambertian::new_with_texture(Box::new(
            perlin_texture.clone(),
        ))),
    )));

    let color = Color::new_from_vec3(&Vec3::new(4., 4., 4.));
    let diffuse_light = DiffuseLight::new_from_vec(&color.color);

    spheres.push(Box::new(SquareXY::new(
        -2.,
        2.,
        0.,
        3.,
        -3.0,
        Box::new(diffuse_light.clone()),
        1,
    )));

    let list: HitableList = HitableList::new(spheres);
    BvhNode::new(list, 0.0, 1.0)
}

pub fn cornell_empty() -> BvhNode {
    let mut walls: Vec<Box<dyn Hitable>> = Vec::default();
    let red = Box::new(Lambertian::new_from_vec(Vec3::new(0.65, 0.05, 0.05)));
    let white = Box::new(Lambertian::new_from_vec(Vec3::new(0.73, 0.73, 0.73)));
    let green = Box::new(Lambertian::new_from_vec(Vec3::new(0.12, 0.45, 0.15)));
    let light = Box::new(DiffuseLight::new_from_vec(&Vec3::new(15., 15., 15.)));

    walls.push(Box::new(SquareXZ::new(
        213.,
        343.,
        227.,
        332.,
        554.,
        light.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        green.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        red.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        white.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        white.clone(),
        1,
    )));
    walls.push(Box::new(SquareXY::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        white.clone(),
        1,
    )));

    let list: HitableList = HitableList::new(walls);
    BvhNode::new(list, 0.0, 1.0)
}

pub fn cornell_simple() -> BvhNode {
    let mut walls: Vec<Box<dyn Hitable>> = Vec::default();
    let red = Box::new(Lambertian::new_from_vec(Vec3::new(0.65, 0.05, 0.05)));
    let white = Box::new(Lambertian::new_from_vec(Vec3::new(0.73, 0.73, 0.73)));
    let green = Box::new(Lambertian::new_from_vec(Vec3::new(0.12, 0.45, 0.15)));
    let light = Box::new(DiffuseLight::new_from_vec(&Vec3::new(15., 15., 15.)));

    walls.push(Box::new(SquareXZ::new(
        213.,
        343.,
        227.,
        332.,
        554.,
        light.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        green.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        red.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        white.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        white.clone(),
        1,
    )));
    walls.push(Box::new(SquareXY::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        white.clone(),
        1,
    )));

    walls.push(Box::new(CornellBox::new(
        &Vec3::new(130., 0., 65.),
        &Vec3::new(295., 165., 230.),
        white.clone(),
    )));

    walls.push(Box::new(CornellBox::new(
        &Vec3::new(265., 0., 295.),
        &Vec3::new(430., 330., 460.),
        white.clone(),
    )));

    let list: HitableList = HitableList::new(walls);
    BvhNode::new(list, 0.0, 1.0)
}

pub fn cornell_transform() -> BvhNode {
    let mut walls: Vec<Box<dyn Hitable>> = Vec::default();
    let red = Box::new(Lambertian::new_from_vec(Vec3::new(0.65, 0.05, 0.05)));
    let white = Box::new(Lambertian::new_from_vec(Vec3::new(0.73, 0.73, 0.73)));
    let green = Box::new(Lambertian::new_from_vec(Vec3::new(0.12, 0.45, 0.15)));
    let light = Box::new(DiffuseLight::new_from_vec(&Vec3::new(7., 7., 7.)));

    walls.push(Box::new(SquareXZ::new(
        113.,
        443.,
        127.,
        432.,
        554.,
        light.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        green.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        red.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        white.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        white.clone(),
        1,
    )));
    walls.push(Box::new(SquareXY::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        white.clone(),
        1,
    )));

    let small_box = Box::new(CornellBox::new(
        &Vec3::new(0., 0., 0.),
        &Vec3::new(165., 165., 165.),
        white.clone(),
    ));
    let small_box = Box::new(RotateY::new(small_box, -18.));
    let small_box = Box::new(Translate::new(small_box, Vec3::new(130., 0., 65.)));

    walls.push(small_box);

    let big_box = Box::new(CornellBox::new(
        &Vec3::new(0., 0., 0.),
        &Vec3::new(165., 330., 165.),
        white.clone(),
    ));
    let big_box = Box::new(RotateY::new(big_box, 15.));
    let big_box = Box::new(Translate::new(big_box, Vec3::new(265., 0., 295.)));
    walls.push(big_box);

    let list: HitableList = HitableList::new(walls);
    BvhNode::new(list, 0.0, 1.0)
}

pub fn cornell_smoke() -> BvhNode {
    let mut walls: Vec<Box<dyn Hitable>> = Vec::default();
    let red = Box::new(Lambertian::new_from_vec(Vec3::new(0.65, 0.05, 0.05)));
    let light_gray = Box::new(Lambertian::new_from_vec(Vec3::new(0.73, 0.73, 0.73)));
    let green = Box::new(Lambertian::new_from_vec(Vec3::new(0.12, 0.45, 0.15)));
    let black = Box::new(Color::new_from_vec3(&Vec3::new(0., 0., 0.)));
    let white = Box::new(Color::new_from_vec3(&Vec3::new(1., 1., 1.)));
    let light = Box::new(DiffuseLight::new_from_vec(&Vec3::new(7., 7., 7.)));

    walls.push(Box::new(SquareXZ::new(
        113.,
        443.,
        127.,
        432.,
        554.,
        light.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        green.clone(),
        1,
    )));
    walls.push(Box::new(SquareYZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        red.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        0.0,
        light_gray.clone(),
        1,
    )));
    walls.push(Box::new(SquareXZ::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        light_gray.clone(),
        1,
    )));
    walls.push(Box::new(SquareXY::new(
        0.,
        555.,
        0.,
        555.,
        555.0,
        light_gray.clone(),
        1,
    )));

    let small_box = Box::new(CornellBox::new(
        &Vec3::new(0., 0., 0.),
        &Vec3::new(165., 165., 165.),
        light_gray.clone(),
    ));
    let small_box = Box::new(RotateY::new(small_box, -18.));
    let small_box = Box::new(Translate::new(small_box, Vec3::new(130., 0., 65.)));
    let small_box = Box::new(ConstantMedium::new(0.005, small_box, white.clone()));

    walls.push(small_box);

    let big_box = Box::new(CornellBox::new(
        &Vec3::new(0., 0., 0.),
        &Vec3::new(165., 330., 165.),
        light_gray.clone(),
    ));
    let big_box = Box::new(RotateY::new(big_box, 15.));
    let big_box = Box::new(Translate::new(big_box, Vec3::new(265., 0., 295.)));
    let big_box = Box::new(ConstantMedium::new(0.005, big_box, black.clone()));
    walls.push(big_box);

    let list: HitableList = HitableList::new(walls);
    BvhNode::new(list, 0.0, 1.0)
}

pub fn final_scene() -> BvhNode {
    let mut scene: Vec<Box<dyn Hitable>> = Vec::default();
    let light = Box::new(DiffuseLight::new_from_vec(&Vec3::new(7., 7., 7.)));
    let white = Box::new(Lambertian::new_from_vec(Vec3::new(0.73, 0.73, 0.73)));
    let ground = Box::new(Lambertian::new_from_vec(Vec3::new(0.48, 0.83, 0.53)));
    for i in 0..20 {
        for j in 0..20 {
            let w = 100f32;
            let x0 = -1000.0 + i as f32 * w;
            let z0 = -1000.0 + j as f32 * w;
            let y0 = 0f32;
            let x1 = x0 + w;
            let y1 = random_float(1., 101.);
            let z1 = z0 + w;
            let p0 = Vec3::new(x0, y0, z0);
            let p1 = Vec3::new(x1, y1, z1);
            scene.push(Box::new(CornellBox::new(&p0, &p1, ground.clone())))
        }
    }

    scene.push(Box::new(SquareXZ::new(
        123.,
        423.,
        147.,
        412.,
        554.,
        light.clone(),
        1,
    )));

    let center1 = Vec3::new(400., 400., 200.);
    let center2 = center1 + Vec3::new(30., 0., 0.);
    let moving_sphere_material = Box::new(Lambertian::new_from_vec(Vec3::new(0.7, 0.3, 0.1)));
    scene.push(Box::new(MovingSphere::new(
        center1,
        center2,
        0.,
        moving_sphere_material,
        50.,
        1.,
    )));

    scene.push(Box::new(Sphere::new(
        Vec3::new(260., 150., 45.),
        50.,
        Box::new(Dielectric::new(1.5)),
    )));
    scene.push(Box::new(Sphere::new(
        Vec3::new(0., 150., 145.),
        50.,
        Box::new(Metal::new_from_vec(Vec3::new(0.8, 0.8, 0.9), 10.0)),
    )));

    let boundary = Box::new(Sphere::new(
        Vec3::new(360f32, 150f32, 145f32),
        70f32,
        Box::new(Dielectric::new(1.5)),
    ));
    scene.push(boundary.clone());
    scene.push(Box::new(ConstantMedium::new(
        0.2,
        boundary.clone(),
        Box::new(Color::new_from_vec3(&Vec3::new(0.2, 0.4, 0.9))),
    )));
    let boundary = Box::new(Sphere::new(
        Vec3::new(0f32, 0f32, 0f32),
        5000f32,
        Box::new(Dielectric::new(1.5)),
    ));
    scene.push(Box::new(ConstantMedium::new(
        0.0001,
        boundary,
        Box::new(Color::new_from_vec3(&Vec3::new(1f32, 1f32, 1f32))),
    )));

    // This one is commented because the textures take to long to load
    // let emat = Box::new(Lambertian::new_with_texture(Box::new(<image_texture>("earthmap.jpg"));
    // scene.push(Box::new(Sphere::new(Vec3::new(400,200,400), 100, emat));
    let pertext = Box::new(NoiseTexture::new());
    scene.push(Box::new(Sphere::new(
        Vec3::new(220f32, 280f32, 300f32),
        80f32,
        Box::new(Lambertian::new_with_texture(pertext)),
    )));

    let mut boxes: Vec<Box<dyn Hitable>> = Vec::default();
    let ns = 1000;
    for _ in 0..ns {
        boxes.push(Box::new(Sphere::new(
            Vec3::new(
                random_float(0f32, 165f32),
                random_float(0f32, 165f32),
                random_float(0f32, 165f32),
            ),
            10f32,
            white.clone(),
        )));
    }

    scene.push(Box::new(Translate::new(
        Box::new(RotateY::new(
            Box::new(BvhNode::new(HitableList::new(boxes), 0.0, 1.0)),
            15f32,
        )),
        Vec3::new(-100f32, 270f32, 395f32),
    )));

    let list: HitableList = HitableList::new(scene);
    BvhNode::new(list, 0.0, 1.0)
}

pub fn scene(scene: &str) -> BvhNode {
    match scene {
        "two_spheres_scene" => two_spheres_scene(),
        "random_scene" => random_scene(),
        "sphere" => sphere(),
        "two_perlin_spheres" => two_perlin_spheres(),
        "earth_sphere" => earth_sphere(),
        "light_scene" => square_light(),
        "cornell_empty" => cornell_empty(),
        "cornell_simple" => cornell_simple(),
        "cornell_transform" => cornell_transform(),
        "cornell_smoke" => cornell_smoke(),
        "final_scene" => final_scene(),
        _ => sphere(),
    }
}
