use vec3::Vec3;

#[derive(Copy, Clone)]
pub struct Ray {
    o: Vec3,
    d: Vec3,
    pub time: f32,
}

impl Ray {
    pub fn new_empty() -> Self {
        Ray {
            o: Vec3::new(0 as f32, 0 as f32, 0 as f32),
            d: Vec3::new(0 as f32, 0 as f32, 0 as f32),
            time: 0.0,
        }
    }

    pub fn new(o: Vec3, d: Vec3, time: f32) -> Self {
        Ray {
            o: Vec3::new_from_vec3(&o),
            d: Vec3::new_from_vec3(&d),
            time,
        }
    }

    pub fn origin(&self) -> Vec3 {
        Vec3::new_from_vec3(&self.o)
    }

    pub fn direction(&self) -> Vec3 {
        Vec3::new_from_vec3(&self.d)
    }

    pub fn point_at_parameter(&self, t: f32) -> Vec3 {
        self.o + self.d * t
    }
}
