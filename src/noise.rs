use vec3::dot;
use vec3::Vec3;
use rand::prelude::{thread_rng, Rng};

fn perlin_generate() -> [Vec3;256] {
    let mut res = [Vec3::new_empty();256];
    let mut rng = thread_rng();
    for i in 0..256 {
        let x = 2. * rng.gen::<f32>() - 1.;
        let y = 2. * rng.gen::<f32>() - 1.;
        let z = 2. * rng.gen::<f32>() - 1.;
        res[i] = Vec3::new(x, y, z).unit_vector();
    }
    res
}

fn permute(p: &mut[u32], n: usize) {
    let mut rng = thread_rng();
    for i in 0..n {
        let target = (rng.gen::<f32>() * (i as f32 + 1.)) as usize;
        let temp = p[target];
        p[target] = p[i as usize];
        p[i as usize] = temp;
    }
}

fn perlin_generate_perm() -> [u32;256] {
    const SIZE: usize = 256;
    let mut res = [0; SIZE];
    for i in 0..SIZE {
        res[i] = i as u32;
    }
    permute(&mut res, SIZE);
    res
}

#[allow(dead_code)]
fn trilinear_interp_f32(c: &[[[f32;2];2];2], u: f32, v: f32, w: f32) -> f32 {
    let mut accum: f32 = 0.0;
    for i in 0..2 {
        for j in 0..2 {
            for k in 0..2 {
                accum += (i as f32 * u + (1-i) as f32 * (1.-u))*
                         (j as f32 * v + (1-j) as f32 * (1.-v))*
                         (k as f32 * w + (1-k) as f32 * (1.-w))*
                         c[i][j][k];
            }
        }
    }
    accum
}

fn trilinear_interp(c: &[[[Vec3;2];2];2],u:f32,v:f32,w:f32) -> f32 {
    let uu = u*u*(3.-2.*u);
    let vv = v*v*(3.-2.*v);
    let ww = w*w*(3.-2.*w);
    let mut res = 0.;
    for i in 0..2 {
        for j in 0..2 {
            for k in 0..2 {
                let weight_v = Vec3::new(u-i as f32, v-j as f32, w-k as f32);
                res += (i as f32 * uu + (1 - i) as f32 *(1. - uu)) *
                        (j as f32 * vv + (1 - j) as f32 *(1. - vv)) *
                        (k as f32 * ww + (1 - k) as f32 *(1. - ww)) *  
                        dot(&c[i][j][k],&weight_v);
            }
        }
    }
    res
}

#[derive(Copy,Clone)]
pub struct BasicPerlin {
    pub rand_float: [Vec3;256],
    pub perm_x: [u32;256],
    pub perm_y: [u32;256],
    pub perm_z: [u32;256],
}

impl BasicPerlin {
    pub fn new() -> Self{
        BasicPerlin {
            rand_float: perlin_generate(),
            perm_x: perlin_generate_perm(),
            perm_y: perlin_generate_perm(),
            perm_z: perlin_generate_perm(), 
        }
    }

    pub fn noise(self, p: &Vec3) -> f32 {
        let u = p.x() - p.x().floor();
        let v = p.y() - p.y().floor();
        let w = p.z() - p.z().floor();

        // let value = 4.;
        // let i = ((value * p.x())) as usize;
        // let j = ((value * p.y())) as usize;
        // let k = ((value * p.z())) as usize;

        let i = f32::floor(p.x()) as usize;
        let j = f32::floor(p.y()) as usize;
        let k = f32::floor(p.z()) as usize;
        let mut c = [[[Vec3::new_empty(); 2]; 2]; 2];
        for di in 0..2 {
            for dj in 0..2 {
                for dk in 0..2 {
                    c[di][dj][dk] =
                        self.rand_float[(self.perm_x[(i + di) & 255] ^ self.perm_y[(j + dj) & 255] ^ self.perm_z[(k + dk) & 255]) as usize]
                }
            }
        };
        trilinear_interp(&c, u, v, w)
    }

    pub fn turb(&self, p: &Vec3, depth: usize) -> f32 {
        let mut accum = 0f32;
        let mut temp = Vec3::new_from_vec3(p);
        let mut weight = 1.;
        for _ in 0..depth {
            accum += weight * self.noise(&temp);
            weight *= 0.5;
            temp = temp * 2.;
        }
        accum.abs()
    }
}