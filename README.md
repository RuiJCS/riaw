# riaw

Project for following the book "Ray Tracing in One Weekend" by Peter Shirley. The code developed in this repository was made in Rust, and the currently active branch is [ray-tracing-the-next-week](https://gitlab.com/RuiJCS/riaw/tree/ray-tracing-the-next-week). The resulting image can be seen below:

![Resulting Image](image.png "Resulting Image")

## To do list

- [ ] Document all the things
- [ ] Add tests?

### Ray Tracing in One Weekend

- [x]  Overview
- [x]  Output an Image
- [x]  The vec3 Class
- [x]  Rays, a Simple Camera, and Background
- [x]  Adding a Sphere
- [x]  Surface Normals and Multiple Objects
- [x]  Antialiasing
- [x]  Diffuse Materials
- [x]  Metal
- [x]  Dielectrics
- [x]  Positionable Camera
- [x]  Defocus Blur
- [ ]  Where Next?

### Ray Tracing: The Next Week

- [x] Motion Blur
- [x] Bounding Volume Hierarchy
- [x] Solid Textures
- [x] Perlin Noise
- [-] Image Texture Mapping **_(Only kinda done)_**
    - This needs to be improved maybe i should replace the image struct with an sdl texture
- [X] Rectangles and Lights
    - In the future try to replace the rectangles with triangles
- [X] Instances
    - Rotations on the Y axis and translations
    - In the future I'll try to add all axis rotation
- [X] Constant volumes
- [X] A scene testing it all together

### Ray Tracing: The Rest of Your Life 

- [ ]  A Simple Monte Carlo Program
- [ ]  One Dimensional MC Integration
- [ ]  MC Integration on the Sphere of Directions
- [ ]  Light Scattering
- [ ]  Importance Sampling Materials
- [ ]  Generating Random Directions
- [ ]  Ortho-normal Bases
- [ ]  Sampling Lights Directly
- [ ]  Mixture Densities
- [ ]  Some Architectural Decisions
- [ ]  Cleaning Up pdf Management.
- [ ]  The Rest of Your Life
